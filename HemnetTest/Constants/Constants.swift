//
//  Constants.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import UIKit

extension UIColor {
    public convenience init(byteRed red: Int, green: Int, blue: Int, alpha: Float = 1.0) {
        self.init(red: CGFloat(red) / 255.0,
                  green: CGFloat(green) / 255.0,
                  blue: CGFloat(blue) / 255.0,
                  alpha: CGFloat(alpha))
    }
    static let green: UIColor = UIColor(byteRed: 65, green: 205, blue: 140)
}

struct StringsUsed {
    static let newDesign: String = "New design"
    static let oldDesign: String = "Old design"
}

struct StoryboardNames {
    static let main: String = "Main"
}

