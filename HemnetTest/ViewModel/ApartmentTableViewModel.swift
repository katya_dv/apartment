//
//  ApartmentViewModel.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import Foundation

class ApartmentTableViewModel {
    
    var apartmentArray = [Apartment]() {
        didSet {
            dataIsLoaded?()
        }
    }
    
    var dataIsLoaded: (()->())?
    var unableToLoad: ((Error)->())?
    
    func loadApartments() {
        NetworkManager.getApartments { [weak self] result in
            switch result {
            case .success(let data):
                self?.apartmentArray = data.listings
            case .failure(let error):
                self?.unableToLoad?(error)
            }
        }
    }
    
    func numberOfRows() -> Int {
        return apartmentArray.count
    }
    
    func numberOfSections() -> Int {
        return 1
    }
    
    func apartment(by row: Int) -> Apartment {
        return apartmentArray[row]
    }
}

