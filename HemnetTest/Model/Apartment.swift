//
//  Apartment.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import Foundation

struct Listing: Codable {
    var listings: [Apartment]
    
    private enum CodingKeys: String, CodingKey {
        case listings = "listings"
    }
}

enum ApartmentType: String, Codable {
    case ActivePropertyListing
    case DeactivatedPropertyListing
}

struct Apartment: Codable {
    var type: ApartmentType
    var id: String
    var askingPrice: String
    var monthlyFee: String
    var municipality: String
    var area: String
    var daysOnHemnet: Int
    var livingArea: Int
    var numberOfRooms: Int
    var streetAddress: String
    var thumbnail: String
    
    private enum CodingKeys: String, CodingKey {
        case type = "listingType"
        case askingPrice = "askingPrice"
        case id = "id"
        case monthlyFee = "monthlyFee"
        case municipality = "municipality"
        case area = "area"
        case daysOnHemnet = "daysOnHemnet"
        case livingArea = "livingArea"
        case numberOfRooms = "numberOfRooms"
        case streetAddress = "streetAddress"
        case thumbnail = "thumbnail"
    }
}
