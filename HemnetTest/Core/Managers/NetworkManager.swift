//
//  NetworkManager.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import Foundation

class NetworkManager {
    class func getApartments(completion:((Result<Listing>) -> Void)?) {
        let urlString = "https://api.myjson.com/bins/rx91b"
        NetworkService.get(withUrlString: urlString) { result in
            switch result {
            case .success(let data):
                let decoder = JSONDecoder()
                decoder.dateDecodingStrategy = .iso8601
                do {
                    let listing = try decoder.decode(Listing.self, from: data)
                    completion?(.success(listing))
                } catch {
                    completion?(.failure(error))
                }
            case .failure(let error):
                completion?(.failure(error))
            }
        }
    }
}
