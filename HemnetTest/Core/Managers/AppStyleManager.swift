//
//  AppStyleManager.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import Foundation

class AppStyleManager: NSObject {
    static let sharedInstance = AppStyleManager()
    private override init() {}
    
    private(set) var isAppStyleNew = true
    
    func updateAppStyle() {
        isAppStyleNew = !isAppStyleNew
    }
}
