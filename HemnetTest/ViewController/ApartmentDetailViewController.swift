//
//  ApartmentDetailViewController.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-29.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import UIKit
import SDWebImage

class ApartmentDetailViewController: UIViewController {
    
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var areaLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var apartmentImageView: UIImageView!
    var apartment: Apartment?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let apartment = apartment else { return }
        
        addressLabel.text = apartment.streetAddress
        
        addressLabel.text = apartment.streetAddress
        priceLabel.text = apartment.askingPrice
        areaLabel.text = String(apartment.livingArea) + " " + UnitArea.squareMeters.symbol
        
        apartmentImageView.sd_setImage(with: URL(string: apartment.thumbnail), placeholderImage: UIImage(named:"default_apartment_img")) { (image, error, type, url) in }
    }

}
