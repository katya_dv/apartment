//
//  ApartmentsViewController.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import UIKit

class ApartmentsViewController: UIViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    @IBOutlet weak private var segmentControl: UISegmentedControl!
    @IBOutlet weak private var loadingIndicator: UIActivityIndicatorView!
    
    var viewModel = ApartmentTableViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        setupViewModel()
        setupSegmentedControl()
    }
    
    @IBAction func appStyleChanged(_ sender: UISegmentedControl) {
        AppStyleManager.sharedInstance.updateAppStyle()
        tableView.reloadData()
    }
    
    private func setupViewModel() {
        viewModel.dataIsLoaded = { [weak self] in
            DispatchQueue.main.async {
                self?.loadingIndicator.stopAnimating()
                self?.tableView.reloadData()
            }
        }
        viewModel.unableToLoad = { [weak self] error in
            self?.loadingIndicator.stopAnimating()
        }
        loadingIndicator.startAnimating()
        viewModel.loadApartments()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib.init(nibName: ApartmentTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: ApartmentTableViewCell.identifier)
    }
    
    private func setupSegmentedControl() {
        segmentControl.setTitle(StringsUsed.newDesign, forSegmentAt: 0)
        segmentControl.setTitle(StringsUsed.oldDesign, forSegmentAt: 1)
        segmentControl.tintColor = UIColor.green
    }
}

extension ApartmentsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ApartmentTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedApartment = viewModel.apartment(by: indexPath.row)
        if selectedApartment.type == .ActivePropertyListing {
            let viewController: ApartmentDetailViewController = UIStoryboard(name: StoryboardNames.main, bundle: nil).instantiateViewController(withIdentifier: String(describing: ApartmentDetailViewController.self)) as! ApartmentDetailViewController
            viewController.apartment = selectedApartment
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension ApartmentsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ApartmentTableViewCell.identifier, for: indexPath) as! ApartmentTableViewCell
        let apartment = viewModel.apartment(by: indexPath.row)
        let isNewDesign = AppStyleManager.sharedInstance.isAppStyleNew
        cell.configureWith(apartment, isNewDesign:isNewDesign)
        return cell
    }
}
