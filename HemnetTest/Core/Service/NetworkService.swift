//
//  NetworkService.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import Foundation

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

class NetworkService {
    static func get(withUrlString urlString: String, completion:((Result<Data>) -> Void)?) {
        guard let url = URL(string: urlString) else {
            print("Error unwrapping URL")
            return
        }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let task = session.dataTask(with: request) { (responseData, response, responseError) in
            guard responseError == nil else {
                completion?(.failure(responseError!))
                return
            }
            
            guard let jsonData = responseData else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                completion?(.failure(error))
                return
            }
            completion?(.success(jsonData))
            
        }
        task.resume()
    }
}
