//
//  ApartmentTableViewCell.swift
//  HemnetTest
//
//  Created by Ekaterina Dvuzhilova on 2018-04-24.
//  Copyright © 2018 Ekaterina Dvuzhilova. All rights reserved.
//

import UIKit
import SDWebImage

class ApartmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak private var addressLabel: UILabel!
    @IBOutlet weak private var locationLabel: UILabel!
    @IBOutlet weak private var priceLabel: UILabel!
    @IBOutlet weak private var areaLabel: UILabel!
    @IBOutlet weak private var pricePerMonthLabel: UILabel!
    @IBOutlet weak private var roomAmountLabel: UILabel!
    @IBOutlet weak private var daysAtHemnetLabel: UILabel!
    @IBOutlet weak private var apartmentImageView: UIImageView!
    @IBOutlet weak private var priceAreaRoomView: UIView!
    
    @IBOutlet weak private var deactivatedTopView: UIView!
    @IBOutlet weak private var removedTextAreaView: UIView!
    @IBOutlet weak private var removedImageAreaView: UIView!
    
    static var identifier: String {
        return String(describing: self)
    }
    
    static var height: CGFloat {
        return 120.0
    }
    
    func configureWith(_ apartment: Apartment, isNewDesign: Bool) {
        addressLabel.text = apartment.streetAddress
        locationLabel.text = apartment.municipality
        priceLabel.text = apartment.askingPrice
        areaLabel.text = String(apartment.livingArea) + " " + UnitArea.squareMeters.symbol
        pricePerMonthLabel.text = apartment.monthlyFee
        roomAmountLabel.text = String(apartment.numberOfRooms) + " rum"
        daysAtHemnetLabel.text = String(apartment.daysOnHemnet) + " dagar"
        apartmentImageView.sd_setImage(with: URL(string: apartment.thumbnail), placeholderImage: UIImage(named:"default_apartment_img")) { (image, error, type, url) in }
        style(for: apartment.type, isNewDesign: isNewDesign)
    }
    
    private func style(for type: ApartmentType, isNewDesign: Bool) {
        switch type {
        case .ActivePropertyListing:
            deactivatedTopView.isHidden = true
            priceAreaRoomView.isHidden = false
            break
        case .DeactivatedPropertyListing:
            deactivatedTopView.isHidden = false
            updateTopView(isNewDesign)
            break
        }
    }
    
    private func updateTopView(_ isNewDesign: Bool) {
        removedImageAreaView.isHidden = !isNewDesign
        removedTextAreaView.isHidden = isNewDesign
        priceAreaRoomView.isHidden = !isNewDesign
    }
}
